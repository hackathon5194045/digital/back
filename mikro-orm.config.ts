import { MikroOrmModuleOptions as Options } from '@mikro-orm/nestjs';

require('dotenv').config();

const config: Options = {
  type: 'postgresql',
  host: process.env.POSTGRES_HOST || 'localhost',
  port: +process.env.POSTGRES_PORT || +5432,
  user: process.env.POSTGRES_USER || 'postgres',
  password: process.env.POSTGRES_PASSWORD,
  dbName: process.env.POSTGRES_NAME,
  entities: ['./dist/src/**/*.entity.js'],
  entitiesTs: ['./src/**/*.entity.ts'],
  debug: true,
  forceUtcTimezone: true,
  timezone: '+03:00',
  registerRequestContext: false,
  migrations: {
    path: './dist/src/migrations',
    pathTs: './src/migrations',
  },
};

export default config;
