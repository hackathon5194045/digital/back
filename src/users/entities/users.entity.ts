import {
  Collection,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';
import { DayHistory } from '../../plan/entities/history.entity';

@Entity({ tableName: 'users' })
export class Users {
  @PrimaryKey()
  id: number;

  @Property()
  firstName: string;

  @Property()
  lastName: string;

  @Property()
  email: string;

  @Property()
  password: string;

  @ManyToMany(() => DayHistory, (dh) => dh.editor)
  edited = new Collection<DayHistory>(this);
}
