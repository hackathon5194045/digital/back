import { EntityManager } from '@mikro-orm/core';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Users } from './entities/users.entity';
import { CreateUsersDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(private readonly em: EntityManager) {}

  async createUser(dto: CreateUsersDto) {
    const canditate = await this.getUserByEmail(dto.email);

    if (canditate) {
      throw new HttpException(
        "User with such email already exist's ",
        HttpStatus.BAD_REQUEST,
      );
    }

    const user = this.em.create(Users, { ...dto });
    await this.em.persistAndFlush(user);
    return user;
  }

  async getUserById(id: number) {
    const user = await this.em.findOne(Users, { id });
    return user;
  }
  async getUserNameById(id: number) {
    const user = await this.em.findOne(
      Users,
      { id },
      { fields: ['firstName', 'lastName'] },
    );
    return user;
  }

  async getUserByIdParams(id: number) {
    const user = await this.em.findOne(
      Users,
      { id },
      { fields: ['email', 'firstName', 'lastName'] },
    );
    return user;
  }

  async getUserByEmail(email: string) {
    const user = await this.em.findOne(
      Users,
      { email },
      { fields: ['email', 'firstName', 'password'] },
    );
    return user;
  }

  async deleteUser(id: number) {
    await this.em.nativeDelete(Users, { id });
    return true;
  }

  async saveUser(user: Users) {
    await this.em.persistAndFlush(user);
    return true;
  }
}
