import { ApiProperty } from '@nestjs/swagger';

export class CreateUsersDto {
  @ApiProperty({ example: 'Ivan', description: 'Name' })
  firstName: string;

  @ApiProperty({ example: 'Ivanov', description: 'Last Name' })
  lastName: string;

  @ApiProperty({ example: 'test@mail.ru', description: 'Email' })
  email: string;

  @ApiProperty({ example: 'test', description: 'Password' })
  password: string;
}
