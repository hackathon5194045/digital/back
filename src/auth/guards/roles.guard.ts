import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { ROLES_KEY } from '../../misc/decorators/roles.decorator';

@Injectable()
export class RoleGuards implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    try {
      const requiredRoles = this.reflector.getAllAndOverride<string[]>(
        ROLES_KEY,
        [context.getHandler(), context.getClass()],
      );
      const authHeader = req.headers.authorization;
      const token = authHeader.split(' ')[1];
      if (!requiredRoles) {
        return true;
      }

      const user = await this.jwtService.verifyAsync(token);
      req.user = user;
      return await user.roles.some((role) =>
        requiredRoles.includes(role.value),
      );
    } catch (error) {
      throw new HttpException('not allowed', HttpStatus.FORBIDDEN);
    }
  }
}
