import { Body, Controller, Post } from '@nestjs/common';
import { CreateUsersDto } from 'src/users/dto/create-user.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/login')
  login(@Body() userDto: CreateUsersDto) {
    return this.authService.login(userDto);
  }
  @Post('/reg')
  registration(@Body() userDto: CreateUsersDto) {
    return this.authService.registration(userDto);
  }
}
