import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { Users } from '../users/entities/users.entity';
import { compare, hash } from 'bcrypt';
import { LoginUserDto } from './dto/user-login.dto';
import { CreateUsersDto } from '../users/dto/create-user.dto';

const expireTime = 5 * 60 * 60 * 1000;

const accessTokenOptions = {
  expiresIn: '5h',
  secret: process.env.ACCESSTOKEN_SECRET || 'secret',
};

const refreshTokenOptions = {
  expiresIn: '7d',
  secret: process.env.REFRESHTOKEN_SECRET || 'secret',
};

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  public async getUserFromAuthentificationToken(token: string) {
    const payload: { id: number } = this.jwtService.verify(token, {
      secret: process.env.JWT_SECRET || 'secret',
    });
    if (!payload.id) {
      throw new UnauthorizedException({ message: 'Invalid Token' });
    }
    return this.usersService.getUserById(payload.id);
  }

  async login(dto: LoginUserDto) {
    try {
      const user = await this.validateUser(dto);
      const payload = {
        id: user.id,
      };
      return {
        user,
        tokens: {
          accessToken: await this.jwtService.signAsync(
            payload,
            accessTokenOptions,
          ),
          refreshToken: await this.jwtService.signAsync(
            payload,
            refreshTokenOptions,
          ),
          expiresIn: new Date().setTime(new Date().getTime() + expireTime),
        },
      };
    } catch (error) {
      if (error instanceof InternalServerErrorException) {
        throw new InternalServerErrorException('Ошибка сервера');
      } else {
        throw error;
      }
    }
  }

  async registration(dto: CreateUsersDto) {
    try {
      const candidate = await this.usersService.getUserByEmail(dto.email);
      if (candidate) {
        throw new BadRequestException(
          'Пользователь с таким email уже зарегестрирован',
        );
      }
      const hashPassword = await hash(dto.password, 5);
      const user = await this.usersService.createUser({
        ...dto,
        password: hashPassword,
      });
      const { password, ...result } = user;
      return result;
    } catch (error) {
      if (error instanceof InternalServerErrorException) {
        throw new InternalServerErrorException('Ошибка сервера');
      } else {
        throw error;
      }
    }
  }

  private async validateUser(dto: LoginUserDto) {
    try {
      const user = await this.usersService.getUserByEmail(dto.email);
      if (!user) {
        throw new BadRequestException('Неправильные email или пароль');
      }
      const passwordEquals = await compare(dto.password, user.password);
      if (user && passwordEquals) {
        const { password, ...result } = user;
        return result;
      }
      throw new BadRequestException('Неправильные email или пароль');
    } catch (error) {
      if (error instanceof InternalServerErrorException) {
        throw new InternalServerErrorException('Ошибка сервера');
      }
      {
        throw error;
      }
    }
  }

  async refreshToken(user: Users) {
    const payload = {
      id: user.id,
    };
    return {
      accessToken: await this.jwtService.signAsync(payload, accessTokenOptions),
      refreshToken: await this.jwtService.signAsync(
        payload,
        refreshTokenOptions,
      ),
      expiresIn: new Date().setTime(new Date().getTime() + expireTime),
    };
  }
}
