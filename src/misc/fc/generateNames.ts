const firstNames = ['Никита', 'Михаил', 'Сергей'];
const lastNames = ['Самаркин', 'Жданов', 'Яцук', 'Беляев'];
const middleNames = ['Сергеевич', 'Леонидович', 'Николаевич', 'Олегович'];

export function generateRandomFullName() {
  const firstName = firstNames[Math.floor(Math.random() * firstNames.length)];
  const lastName = lastNames[Math.floor(Math.random() * lastNames.length)];
  const middleName =
    middleNames[Math.floor(Math.random() * middleNames.length)];
  return `${lastName} ${firstName} ${middleName}`;
}
