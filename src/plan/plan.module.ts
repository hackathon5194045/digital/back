import { Module } from '@nestjs/common';
import { PlanController } from './plan.controller';
import { PlanService } from './plan.service';
import { UsersModule } from '../users/users.module';
import { JwtModule } from '@nestjs/jwt';

@Module({
  providers: [PlanService],
  controllers: [PlanController],
  exports: [PlanService],
  imports: [UsersModule, JwtModule],
})
export class PlanModule {}
