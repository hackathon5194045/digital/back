import {
  Collection,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';
import { DayPlan } from './day-plan.entity';
import { Users } from '../../users/entities/users.entity';

@Entity({ tableName: 'history' })
export class DayHistory {
  @PrimaryKey()
  id: number;

  @Property({ default: 0 })
  plannedDebit: number;

  @Property()
  result: number;

  @ManyToMany(() => Users, (u) => u.edited, { owner: true })
  editor = new Collection<Users>(this);

  @Property({ type: 'bigint' })
  time: string;

  @Property()
  adjustment: number;

  @ManyToOne({ entity: () => DayPlan, nullable: true })
  plan: DayPlan;
}
