import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';
import { BCDNG } from '../../cdng/entities/cdng.entity';
import { Debit } from '../../cdng/entities/debit.entity';
import { DayHistory } from './history.entity';

@Entity({ tableName: 'dayPlan' })
export class DayPlan {
  @PrimaryKey()
  id: number;

  @Property({ default: 0 })
  plannedDebit: number;

  @Property()
  day: number;

  @Property()
  month: number;

  @OneToMany(() => DayHistory, (h) => h.plan)
  history = new Collection<DayHistory>(this);

  @ManyToOne({ entity: () => BCDNG })
  cdng: BCDNG;

  @OneToMany(() => Debit, (d) => d.dayPlan)
  dayDebits = new Collection<Debit>(this);
}
