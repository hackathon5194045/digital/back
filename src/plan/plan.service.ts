import { EntityManager } from '@mikro-orm/core';
import { Injectable } from '@nestjs/common';
import { DayPlan } from './entities/day-plan.entity';
import { BCDNG } from '../cdng/entities/cdng.entity';
import { UpdatePlanDto } from './dto/update-plan.dto';
import { GetPlanDto } from './dto/get-plan.dto';
import { UsersService } from '../users/users.service';
import { DayHistory } from './entities/history.entity';

@Injectable()
export class PlanService {
  constructor(
    private em: EntityManager,
    private readonly usersService: UsersService,
  ) {}

  async createPlan(cdng: BCDNG, day?: number, month?: number) {
    const date = new Date();
    const dayPlan = this.em.create(DayPlan, {
      day: day || date.getDate(),
      month: month || date.getMonth() + 1,
      cdng,
    });
    await this.em.persistAndFlush(dayPlan);
    return dayPlan;
  }

  async updatePlan(uid: string, dto: UpdatePlanDto, userId: number) {
    const cdng = await this.em.findOne(BCDNG, { uid });
    const plan = await this.em.findOne(DayPlan, {
      cdng,
      day: dto.day,
      month: dto.month,
    });
    await this.em.nativeUpdate(
      DayPlan,
      { cdng, day: dto.day, month: dto.month },
      { plannedDebit: dto.newDebit },
    );
    const adjustment = dto.newDebit - plan.plannedDebit;
    await this.createHistory(
      plan.plannedDebit,
      userId,
      adjustment,
      plan,
      dto.newDebit,
    );
    return { status: 200, message: 'OK' };
  }

  async getPlanData(uid: string, dto: GetPlanDto) {
    const cdng = await this.em.findOne(BCDNG, { uid });
    try {
      const plan = await this.em.findOneOrFail(
        DayPlan,
        { cdng, day: dto.day, month: dto.month },
        {
          populate: ['dayDebits.debit'],
          fields: ['plannedDebit', 'dayDebits.debit'],
        },
      );
      const totalDebit = plan.dayDebits.reduce(
        (sum, item) => sum + item.debit,
        0,
      );
      const history = await this.em.find(
        DayHistory,
        { plan: plan },
        {
          fields: [
            'adjustment',
            'editor',
            'plannedDebit',
            'time',
            'editor.firstName',
            'editor.lastName',
            'result',
          ],
          populate: ['editor.firstName', 'editor.lastName'],
        },
      );
      return { totalDebit, plannedDebit: plan.plannedDebit, history };
    } catch {
      const plan = await this.createPlan(cdng, dto.day, dto.month);
      const totalDebit = plan.dayDebits.reduce(
        (sum, item) => sum + item.debit,
        0,
      );
      return { totalDebit, plannedDebit: plan.plannedDebit };
    }
  }

  private async createHistory(
    oldDebit: number,
    userId: number,
    adjustment: number,
    plan: DayPlan,
    result: number,
  ) {
    const date = new Date(Date.now()).getTime();
    const user = await this.usersService.getUserById(userId);
    const history = this.em.create(DayHistory, {
      adjustment,
      editor: user,
      plan: plan,
      plannedDebit: oldDebit,
      time: date.toString(),
      result,
    });

    await this.em.persistAndFlush(history);
  }
}
