import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { PlanService } from './plan.service';
import { UpdatePlanDto } from './dto/update-plan.dto';
import { GetPlanDto } from './dto/get-plan.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

@Controller('plan')
export class PlanController {
  constructor(private readonly planService: PlanService) {}

  @UseGuards(JwtAuthGuard)
  @Put('/:uid')
  async updateDebitPlan(
    @Param('uid') uid: string,
    @Body() dto: UpdatePlanDto,
    @Req() req,
  ) {
    return await this.planService.updatePlan(uid, dto, req.user.id);
  }
  @Post('/:uid')
  async getDebitPlan(@Param('uid') uid: string, @Body() dto: GetPlanDto) {
    return await this.planService.getPlanData(uid, dto);
  }
}
