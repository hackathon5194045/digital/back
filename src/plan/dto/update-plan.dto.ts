export class UpdatePlanDto {
  day: number;
  month: number;
  newDebit: number;
}
