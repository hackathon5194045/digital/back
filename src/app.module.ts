import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module, Scope } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { CdngModule } from './cdng/cdng.module';
import { PlanModule } from './plan/plan.module';
import config from 'mikro-orm.config';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    MikroOrmModule.forRoot({
      ...config,
      scope: Scope.REQUEST,
    }),
    ScheduleModule.forRoot(),
    UsersModule,
    AuthModule,
    CdngModule,
    PlanModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
