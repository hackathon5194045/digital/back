import { EntityManager } from '@mikro-orm/postgresql';
import { Injectable, Scope } from '@nestjs/common';
import { v4 } from 'uuid';
import { BCDNG } from './entities/cdng.entity';
import { Debit } from './entities/debit.entity';
import { generateRandomFullName } from '../misc/fc/generateNames';
import { PlanService } from '../plan/plan.service';
import { DayPlan } from '../plan/entities/day-plan.entity';
import { DayHistory } from '../plan/entities/history.entity';

@Injectable({ scope: Scope.DEFAULT })
export class CdngService {
  constructor(
    private em: EntityManager,
    private readonly planService: PlanService,
  ) {}

  async createCDNG() {
    const uid = v4();
    const name = 'ЦДНГ-' + ((await this.em.count(BCDNG)) + 1).toString();
    const master = generateRandomFullName();
    const masterPhone = Math.floor(Math.random() * 1000000);
    const bush = Math.floor(Math.random() * 5) + 1;
    const CDNG = this.em.create(BCDNG, {
      uid,
      name,
      master,
      masterPhone,
      bush,
    });

    await this.em.persistAndFlush(CDNG);
    await this.planService.createPlan(CDNG);
    await this.generateDebitByCDNG(CDNG);
    return { status: 200, message: 'OK' };
  }

  async getAllCDNG() {
    const cdng = this.em.find(
      BCDNG,
      {},
      {
        fields: ['name'],
      },
    );
    return cdng;
  }

  async getCDNGByUid(uid: string) {
    const cdng = this.em.findOne(
      BCDNG,
      { uid },
      { fields: ['name', 'master', 'masterPhone'] },
    );
    return cdng;
  }

  async getDebitsByUid(uid: string) {
    const cdng = await this.em.findOne(BCDNG, { uid: uid });
    const debits = await this.em.find(Debit, { cdng }, { fields: ['debit'] });
    return debits;
  }

  async generateDebit() {
    const cdngs = await this.em.find(BCDNG, {}, { fields: ['uid'] });
    const dat = new Date();
    let superList = [];

    for (const cdng of cdngs) {
      const plan = await this.em.findOne(DayPlan, {
        cdng,
        day: dat.getDate(),
        month: dat.getMonth() + 1,
      });
      for (let i = 0; i < 12; i++) {
        const maxDebitValue = Math.floor(Math.random() * (4e4 - 2e4 + 1)) + 2e4;
        const debit1 = this.em.create(Debit, {
          cdng: cdng,
          date: new Date().setHours(i).toString(),
          debit: Math.random() * maxDebitValue,
          dayPlan: plan,
        });
        superList.push(debit1);
      }
    }
    await this.em.persistAndFlush(superList);
  }

  private async generateDebitByCDNG(cdng: BCDNG) {
    const dat = new Date();
    let superList = [];

    const plan = await this.em.findOne(DayPlan, {
      cdng,
      day: dat.getDate(),
      month: dat.getMonth() + 1,
    });
    console.log(plan);
    for (let i = 0; i < 12; i++) {
      const maxDebitValue = Math.floor(Math.random() * (4e4 - 2e4 + 1)) + 2e4;
      const debit1 = this.em.create(Debit, {
        cdng: cdng,
        date: new Date().setHours(i).toString(),
        debit: Math.random() * maxDebitValue,
        dayPlan: plan,
      });
      superList.push(debit1);
    }
    await this.em.persistAndFlush(superList);
  }

  async dataToGraph(uid: string) {
    const cdng = await this.em.findOne(BCDNG, { uid });
    const plan = await this.em.findOne(
      DayPlan,
      { cdng },
      { populate: ['dayDebits'] },
    );
    const history = await this.em.find(
      DayHistory,
      { plan },
      { fields: ['plannedDebit', 'result', 'time'] },
    );

    let accumulatedRes = 0;
    const planData = history.map((item, index) => {
      const time = parseFloat(item.time);

      accumulatedRes = item.result;
      return {
        id: index,
        time,
        data: accumulatedRes,
      };
    });
    // console.log(plan.dayDebits);
    // console.log(plan.plannedDebit);

    let accumulatedDebit = 0; // Переменная для накопления суммы debit
    const factData = plan.dayDebits.map((item, index) => {
      const id = index;
      const time = parseFloat(item.date);
      accumulatedDebit += item.debit; // Накопление суммы debit

      return {
        id,
        time,
        calculated_debit: accumulatedDebit,
      };
    });
    return { factData, planData };
  }
}
