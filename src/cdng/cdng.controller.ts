import { Controller, Get, Param, Post } from '@nestjs/common';
import { CdngService } from './cdng.service';

@Controller('cdng')
export class CdngController {
  constructor(private readonly cdngService: CdngService) {}

  @Post()
  async createeCDNG() {
    return await this.cdngService.createCDNG();
  }

  @Post('/debits')
  async createeDebitForALL() {
    return await this.cdngService.generateDebit();
  }
  @Get('/debits/:uid')
  async getDebitForALL(@Param('uid') uid: string) {
    return await this.cdngService.getDebitsByUid(uid);
  }

  @Get()
  async getAllCDNG() {
    return await this.cdngService.getAllCDNG();
  }

  @Get('/:uid')
  async getCDNGData(@Param('uid') uid: string) {
    return await this.cdngService.getCDNGByUid(uid);
  }
  @Get('/graph/:uid')
  async getGData(@Param('uid') uid: string) {
    return await this.cdngService.dataToGraph(uid);
  }
}
