import { Module } from '@nestjs/common';
import { CdngController } from './cdng.controller';
import { CdngService } from './cdng.service';
import { PlanModule } from '../plan/plan.module';

@Module({
  controllers: [CdngController],
  providers: [CdngService],
  imports: [PlanModule],
  exports: [CdngService],
})
export class CdngModule {}
