import { Entity, ManyToOne, PrimaryKey, Property } from '@mikro-orm/core';
import { BCDNG } from './cdng.entity';
import { DayPlan } from '../../plan/entities/day-plan.entity';

@Entity({ tableName: 'debit' })
export class Debit {
  @PrimaryKey()
  id: number;

  @Property({ type: 'bigint' })
  date: string;

  @Property({ type: 'double' })
  debit: number;

  @ManyToOne({ entity: () => BCDNG })
  cdng: BCDNG;

  @ManyToOne({ entity: () => DayPlan })
  dayPlan: DayPlan;
}
