import {
  Collection,
  Entity,
  OneToMany,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';
import { DayPlan } from '../../plan/entities/day-plan.entity';
import { Debit } from './debit.entity';

@Entity({ tableName: 'CDNG' })
export class BCDNG {
  @PrimaryKey()
  uid: string;

  @Property()
  name: string;

  @Property()
  master: string;

  @Property()
  masterPhone: number;

  @Property()
  bush: number;

  @OneToMany(() => DayPlan, (dp) => dp.cdng)
  dayPlan = new Collection<DayPlan>(this);

  @OneToMany(() => Debit, (d) => d.cdng)
  debits = new Collection<Debit>(this);
}
